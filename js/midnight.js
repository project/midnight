Drupal.behaviors.midnightBehaviors = function(context){

  $('#nav ul li', context).hover(function() {
    $(this).toggleClass('hover');
  }, function() {
    $(this).toggleClass('hover');
  });

  $('a.toggleInfo', context).click(function(e){
    e.preventDefault();
   // $(this).parent().toggleClass('show-info');
  });
  $('a.toggleInfo, .user-info, .comment-info', context).hover(function(e){
      $(this).parent().toggleClass('show-info');
     }, function() {
       $(this).parent().toggleClass('show-info');
     });

	$('fieldset.collapsible .fieldset-title', context).click(hideFieldset);

  function hideFieldset() {
    var $content = $(this).parent().children('.fieldset-body');
    if($content.is(':hidden')){
      $(this).parent().removeClass('collapsed');
      $content.show();
    }else{
      $content.hide();
      $(this).parent().addClass('collapsed');
    }
  }
};