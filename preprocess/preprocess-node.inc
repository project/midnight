<?php

/**
 * Implementation of theme_preprocess_HOOK().
 * Passes varables to the node templates.
 *
 * @return $vars
 */

// Prepare the arrays to handle the classes and ids for the node container.
if(!isset($vars['node']->attributes)) {
  $vars['node_attributes'] = array();
}
else {
  $vars['node_attributes'] = $vars['node']->attributes;
}

// Add an id to allow the styling of a specific node.
$vars['node_attributes']['id'] = 'node-' . $vars['type'] . '-' . $vars['nid'];

// adding a node class to all nodes
$vars['node_attributes']['class'][] = 'node';

// Add a class to allow styling of nodes of a specific type.
$vars['node_attributes']['class'][] = $vars['type'] . '-ntype';

// Add a class to allow styling based on if a node is showing a teaser or the 
// whole thing.
if ($vars['teaser']) {
  $vars['node_attributes']['class'][] = 'teaser-view';
}
else {
  $vars['node_attributes']['class'][] = 'full-view';
}

//support for skinr
if($vars['skinr']) {
  $vars['node_attributes']['class'][] = $vars['skinr'];
}

// Adding node_inner region to node template
if($node_inner){
	theme('blocks', $node_inner);
}

// Formatting the date
if($vars['submitted']) {
  // Make individual variables for the parts of the date.
  $date_day = format_date($vars['node']->created, 'custom', 'j');
  $date_month = format_date($vars['node']->created, 'custom', 'F');
  $date_year = format_date($vars['node']->created, 'custom', 'Y');

  $vars['submitted'] = '<span class="date"><strong>'. $date_month .'</strong> ';
  $vars['submitted'] .=  $date_day .', ';
  $vars['submitted'] .=  $date_year .'</span>';
} elseif(!$vars['submitted'] && $vars['node']->comment != 0) {
  $vars['node_attributes']['class'][] = 'no-date';
}

// user stuff

if($vars['node']->comment != 0 && user_access('access comments')) {
    $vars['comment_link'] = l($vars['node']->comment_count, 'node/'. $vars['node']->nid, array('fragment' => 'comments', 'attributes' => array('class' => 'comment-count')));
} else {
  $vars['node_attributes']['class'][] = 'cant-comment';
}

$nodeuser = user_load($vars['uid']);

if($vars['uid'] != 0) {
  $vars['name_short'] = l(drupal_substr($nodeuser->name, 0, 6), 'user/'. $vars['uid'], array('attributes' => array('class' => 'toggleInfo')));
  $vars['node_attributes']['class'][] = 'has-username';
}

if($vars['node']->picture) {
  $user_picture = '<img src="/'. $vars['node']->picture .'" alt="'. $nodeuser->name .'"/>';
} else {
  $user_picture = '<img src="/'. path_to_theme() .'/images/userphoto.png" alt="'. $nodeuser->name .'"/>';
}

$vars['user_info'] = '<div class="user-info">';
$vars['user_info'] .= $user_picture;
$vars['user_info'] .= '<span class="user-data"><strong>'. $vars['name'] .'</strong><br/>';
if($vars['uid'] != 0) {
  $vars['user_info'] .= t('Member since:') .'<br/>'. format_date($nodeuser->created, 'custom', 'M j, Y') .'</span>';
  if(user_access('access user profiles')) {
    $vars['user_info'] .= l(t('view profile'), 'user/'. $vars['uid'],     array('attributes' => array('class' => 'profile')));
  }
}
$vars['user_info'] .= '</div>';



// Crunch all the attributes together into a single string to be applied to 
// the node container.
$vars['attributes'] = theme('render_attributes', $vars['node_attributes']);